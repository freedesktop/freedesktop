<!--
    Use this template to request a legacy account for freedesktop.org. You
    should only use this form if you have been specifically requested by a
    project maintainer to do so.

    For projects already hosted on gitlab.freedesktop.org, you may just
    request access directly from a maintainer, and you do not need to use
    this form.

    This will request an account on our legacy SSH infrastructure; we need
    the information below to create a separate account for you. This account
    is not linked to GitLab; changes in (e.g.) your SSH keys or email address
    will not be synced between the two systems.

    After you have filled this template, please send a link to the issue to
    the project maintainers so they can approve it and we can create your
    account. You will also need to have attached your SSH and GPG (yes, GPG)
    public keys to this issue.

    Thanks!
-->

Hi! Please create me a legacy freedesktop.org account to commit to (a) project(s) which have not yet migrated to GitLab hosting. Here are the details you need to create this account:

**Email address** (if different from GitLab registered address):

**Desired username** (if different from GitLab username):

**Project(s) to have access to**:

<!-- These are individual item checkboxes; you can tick them off as each is complete:
     - [ ] this is not completed
     - [x] this is completed
-->

 **Account creation checklist**:
- [ ] Issue template fully filled out <!-- you should be able to cross this one off yourself -->
- [ ] SSH and GPG keys attached to this issue <!-- remember to attach both! -->
- [ ] Request approved by project maintainer <!-- send them a link when created! -->
- [ ] Account created by fd.o admin

/label ~"Legacy account requests"
